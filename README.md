# rpi_mcp2517
This is library for MCP2517FD CAN FD controller interface with Raspberry Pi PICO (RP2040). This is needed for Control Engineer's project "AGV". 


## Contents
MCP2517 uses SPI to communicate with microcontroller. This library will be able to interface all of the microcontrolers once proxy class is changed.

## Author
Maksymilian Grabowy